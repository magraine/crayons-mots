<?php
/**
 * Options du plugin Crayons pour mots-clésau chargement
 *
 * @plugin     Crayons pour mots-clés
 * @copyright  2014
 * @author     Matthieu Marcillaud
 * @licence    GNU/GPL
 * @package    SPIP\Crayonsmots\Options
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * Obtenir la liste des identifiants de mots clés d'un groupe lié à un article
 *
 * @uses valeur_champ_mots_objet()
 * 
 * @param string $table
 *     Nom de la table
 * @param string $ids
 *     Identifiants 'A-B' ou
 *     - A correspond à id_article
 *     - B correspond à id_groupe
 * @param string $champ
 *     Champ concerné (ici nom du contrôleur de crayon)
 * @return array
 *     Liste des identifiants de mots cochés
**/ 
function valeur_champ_mots_article($table, $ids, $champ) {
	return valeur_champ_mots_objet($table, $ids, $champ);
}

/**
 * Obtenir la liste des identifiants de mots clés d'un groupe lié à un objet
 *
 * @uses valeur_champ_mots_objet()
 * 
 * @param string $table
 *     Nom de la table
 * @param string $ids
 *     Identifiants 'A-B' ou
 *     - A correspond à id_objet
 *     - B correspond à id_groupe
 * @param string $champ
 *     Nom du contrôleur, qui contient le type de notre objet (yy dans 'mots_yy')
 * @return array
 *     Liste des identifiants de mots cochés
**/ 
function valeur_champ_mots_objet($table, $ids, $champ) {
	list($id_objet, $id_groupe) = explode('-', $ids);
	list(, $objet) = explode('_', $champ);

	$valeurs = sql_allfetsel("m.id_mot", "spip_mots AS m, spip_mots_liens AS ml", array(
		"m.id_groupe=" . sql_quote($id_groupe),
		"m.id_mot = ml.id_mot",
		"ml.id_objet=".sql_quote($id_objet),
		"ml.objet=".sql_quote($objet)
	));
	$valeurs = array_map('array_shift', $valeurs);

	return $valeurs;
}


/**
 * La révision du crayon mots_article doit supprimer ou ajouter des liaisons de mots clés
 *
 * @uses mots_objet_revision()
 * 
 * @param string $ids
 *     Identifiants 'A-B' ou
 *     - A correspond à id_objet
 *     - B correspond à id_groupe
 * @param array $colonnes
 *     Données à enregistrer, couple (champ => valeur)
 * @param string $type_objet
 *     Nom de la table
 * @return bool
**/
function mots_article_revision($ids, $colonnes, $type_objet) {
	return mots_objet_revision($ids, $colonnes, $type_objet, 'mots_article');
}

/**
 * Révision générique d'un crayon mots_xx qui doit supprimer
 * ou ajouter des liaisons de mots clés
 *
 * @uses valeur_champ_mots_objet()
 * 
 * @param string $ids
 *     Identifiants 'A-B' ou
 *     - A correspond à id_objet
 *     - B correspond à id_groupe
 * @param array $colonnes
 *     Données à enregistrer, couple (champ => valeur)
 * @param string $type_objet
 *     Nom de la table
 * @param string $type_liaison
 *     type d'objet de liaison (ex: article)
 * @return bool
**/
function mots_objet_revision($ids, $colonnes, $type_objet, $champ = '') {

	if (!$champ) return false;

	list($id_objet, $id_groupe) = explode('-', $ids);
	list(, $type_liaison) = explode('_', $champ);

	// actuellement en bdd
	$old = valeur_champ_mots_objet($type_objet, $ids, $champ);
	// ceux qu'on veut maintenant
	$new = array_filter(explode(',', $colonnes[$champ]), 'is_numeric');
	// les mots à supprimer
	$del = array_diff($old, $new);
	// les mots à ajouter
	$add = array_diff($new, $old);

	include_spip('action/editer_liens');
	if ($del) {
		objet_dissocier(array('mot'=>$del), array($type_liaison => $id_objet));
	}
	if ($add) {
		objet_associer(array('mot'=>$add), array($type_liaison => $id_objet));
	}

	return true;
}


// Idem pour le controleur tags

/**
 * Obtenir la liste des identifiants de mots clés d'un groupe lié à un article
 *
 * @uses valeur_champ_mots_objet()
 * 
 * @param string $table
 *     Nom de la table
 * @param string $ids
 *     Identifiants 'A-B' ou
 *     - A correspond à id_article
 *     - B correspond à id_groupe
 * @param string $champ
 *     Champ concerné (ici nom du contrôleur de crayon)
 * @return array
 *     Liste des identifiants de mots cochés
**/ 
function valeur_champ_tags_article($table, $ids, $champ) {
	return valeur_champ_mots_objet($table, $ids, $champ);
}

/**
 * La révision du crayon mots_article doit supprimer ou ajouter des liaisons de mots clés
 *
 * @uses mots_objet_revision()
 * 
 * @param string $ids
 *     Identifiants 'A-B' ou
 *     - A correspond à id_objet
 *     - B correspond à id_groupe
 * @param array $colonnes
 *     Données à enregistrer, couple (champ => valeur)
 * @param string $type_objet
 *     Nom de la table
 * @return bool
**/
function tags_article_revision($ids, $colonnes, $type_objet) {
	return mots_objet_revision($ids, $colonnes, $type_objet, 'tags_article');
}
