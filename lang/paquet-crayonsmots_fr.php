<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'crayonsmots_description' => 'Offre des crayons capables de sélectionner des mots clés (de certains groupes de mots)',
	'crayonsmots_nom' => 'Crayons pour mots-clés',
	'crayonsmots_slogan' => 'Sélectionner des mots clés avec les crayons',
);

?>