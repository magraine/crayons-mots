<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucun_mot_present' => 'Aucun mot sélectionné…',

	// C
	'crayonsmots_titre' => 'Crayons pour mots-clés',

);

?>
